resource "local_file" "testing_file" {
  content  = <<-EOT
  ---
  data: foobar
  EOT
  filename = "./.cache/file.yaml"
}
